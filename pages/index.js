import Link from 'next/link'
import Main from '../components/Main'

const Index = () => {
    return (
        <div>
            <header>
                <Link href='/'>
                    Main
                </Link>
                <Link href='/users'>
                    Users
                </Link>
                <Link href='/profile/profile'>
                    Users
                </Link>
            </header>

            <Main />
        </div>
    )
}

export default Index

// export default function App()
